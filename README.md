# mask

a bike / covid19 mask

![](https://gitlab.com/libre_patterns/mask/-/raw/master/images/v0.1.png)

release under [CC by-sa 4.0](http://creativecommons.org/licenses/by-sa/4.0/)

![](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)